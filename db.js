var config = require('./config') 
var mongoose = require('mongoose') 
var uri = 'mongodb://'+config.db_host+':'+config.db_port+'/'+config.db_name
var options = {
  user: config.db_user,
  pass: config.db_pass
}

mongoose.connect(uri, options, function () {  
	console.log('Nawiązano połączenie z mongodb.') 
}) 
module.exports = mongoose